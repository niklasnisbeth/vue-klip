Spil udsnit af en lyd
====

Fordi jeg skal bruge noget der minder om det her i en anden kontekst og gerne ville lære lidt mere om Vue har jeg bygget en lille web app, hvor man indlæse en lydfil og grafisk vælge et udsnit og afspille det. Der en knap nederst som åbner en filvælger, eller man kan indlæse et lille eksempel ved at klikke på linket. Derefter kan man flytte på de to sort håndtag for at vælge et mindre udsnit af filen.

Det er mit allerførste Vue-projekt. Der er sikkert en del ting jeg ikke har forstået. Det er lavet til lejligheden over et par aftner når jeg lige haft tid.

Det er en flad HTML-fil med inlinet stylesheet og Javascript, fordi det er min definition på simpel. Første skridt mod en rigtigt applikation ville naturligvis være at sætte noget bundling op, nok Webpack, for også at bedre at kunne køre noget linting/prettier, polyfills osv. Men til et hurtigt hack kan jeg godt lide det her workflow, med eksplicitte reloads og en/få filer jeg bare kan tyre i en undermappe i den Caddy-server jeg alligevel har på internettet - [rven.se/vue](https://rven.se/vue).

Det jeg primært var interesseret i at finde ud af, var hvordan man laver views/viewmodels, og hvordan man håndterer kommunikationen imellem dem.

Jeg fravalgte bevidst at bruge Vuex, fordi jeg gerne ville koncentrere mig om Vue. Retrospektivt sparede jeg sikkert ikke så meget ved det, og Vuex ligner egentlig et meget godt library til statehåndtering. Jeg er endt med bare at bruge et simpel delt datastoreobjekt, med eksplicitte setterfunktioner. Det interessante er at holde applikationsstate som er relevant for flere komponenter væk fra viewet, og sørge for at komponenter opdaterer storet og ikke sig selv direkte når der er ændringer i staten. Jeg er nogenlunde tilfreds den arbejdsdeling jeg har mellem Vue-komponenter og storet, men det var ikke altid ligetil at afgøre hvad der skal i en watcher og hvad der skal i en computed property. Jeg forsøgte mig også med custom events, men kunne ikke rigtigt få det til at virke som jeg ville have det. Udover at inkludere f.eks. Vuex ville jeg nok foretrække at dele storet op i flere hvis det var et projekt der skulle skaleres op.

Jeg har brugt et canvastag til at tegne waveformen (det er nok den bedste måde at gøre det på), men DOM-elementer til de widgets, man vælger hvilket udsnit der skal afspilles. Min første tanke var at lave dem sammen med waveformen i eet canvas-tag, men det ender hurtigt med man reimplementerer meget af den funktionalitet, der allerede findes i browseren. Hvis programmet voksede og der endte med at være flere widgets i det samme canvastag ville man nok ende et sted imellem at lave et halvsløjt UI-toolkit og en kodebase der var ret svær at ændre i. Og så er det rart at kunne bruge CSS til at style istedet for at skulle sætte farver osv. i den imperative canvaskode.

Ang. CSS, så bruger jeg her et framework der hedder Tachyons. Det er det approach man kalder functional eller atomic CSS, og det ligner det glade vanvid første gang man ser det - men der er en mening bag, og jeg er begyndt at synes at det i mange tilfælde er den "rigtige" måde at håndtere CSS på. Det forklarer jeg gerne nærmere om hvis vi mødes.

Der er et par "kendte" bugs:

* Det hakker når man rykker startpunktet til højre, og endepunktet til venstre. Det er en bug i mousemove-handleren, som jeg ved hvordan jeg skulle fikse men ikke lige fik lavet inden lukke tid. Det er heller ikke optimalt at man trække-operation afsluttes hvis man bevæger musen udenfor elementet, håndtagene bor i.

* Det ville nok være mere korrekt at tegne waveformen i et skjult canvaselement og blitte den ind, hvorgang udsnittet ændrer sig, fremfor at tegne den til skærmen og gemme den (l. 195, 'store.buffer'-watcher), og så straks derefter overskrive den (l. 199, 'store.selection'-watcher).

* Canvastagget har en fast bredde, så appen er ikke responsiv
